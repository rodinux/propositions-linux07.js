![Logo Linux07](https://blog.linux07.fr/wp-content/uploads/2022/12/new_banniere2-linux07.png "Logo Linux07")
## Qui sommes-nous ?
www.linux07.aufildudoux.fr
///
Passionés par les outils numériques et de la richesse des valeurs portées par GNU/Linux, nous désirons promouvoir et défendre le logiciel libre.
///
### But de l'association :
- promouvoir et découvrir la culture et l'utilisation des logiciels libres et des systèmes d'exploitations GNU/Linux auprès du public et des institutions
- ainsi que la mise à disposition et les aprentissages des outils numériques libres.

===

## Projets
### organiser des manifestations pour sensibiliser aux logiciels libres.
Cette journée serait ouverte
- au public,
- aux associations
- aux écoles
///
### avec la vocation de faire connaître les systèmes d'exploitation GNU/Linux
- son histoire et sa philosophie
- des activités,
- des interventions,
- des ateliers,
- de courts documentaires et des ressources.

===
## Quelques idées en vrac
===

- Jeux vidéo logiciels libres.

- Documentaires sur l'histoire du logiciel libre.

- L'installation d'une distribution libre sur son ordinateur (voir les distributions GNU/LINUX recommandées du FSF'S).

===
### Initiation à l'environnement GNU/Linux
bases et rudiments de l'utilisation des commandes du shell (bash)
///
![Terminal](https://blog.linux07.fr/wp-content/uploads/2018/07/terminal-handyrod.png "Terminal")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support1.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support2.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support3.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support4.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support5.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support6.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support7.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support8.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support9.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support10.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support11.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support12.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support13.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support14.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support15.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support16.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support17.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support18.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support19.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support20.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support22.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support23.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support24.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support25.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support26.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support27.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support28.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support29.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support30.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support31.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support32.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support33.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support34.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support35.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support36.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support37.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support38.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support39.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support40.png "Cours shell")
///
![Cours shell](https://blog.linux07.fr/wp-content/uploads/2018/07/support41.png "Cours shell")

===
### Organisez un hackathon logiciel libre.
///
Un hackathon est un événement où des développeurs se réunissent pour faire de la programmation informatique collaborative. Le terme est un mot-valise constitué de hack et marathon.
///
C’est à l'origine un rassemblement de développeurs organisés par équipe autour de porteurs de projet avec l’objectif de produire un prototype d’application en quelques heures.
Sous forme de concours chronométré, l’équipe gagnante est généralement désignée par un jury à l’issue du temps imparti.
La référence au Marathon se justifie par le travail sans interruption des développeurs pendant deux jours, généralement lors d'un week-end.

===

### Présentation des Jerry DIT
Do It Together
///
![Jerry](https://blog.linux07.fr/wp-content/uploads/2018/07/jerrytux.jpg "Jerry")
///
Un atelier Jerry permet à tous de découvrir comment fonctionne un ordinateur de manière amusante.
Les participants prennent conscience qu'un ordinateur n'est pas qu'une boite noire. On peut récupérer ces vielles pièces et redonner vie à un ordinateur destiné à la casse !
///
![Jerry](https://blog.linux07.fr/wp-content/uploads/2018/07/futurjerry.png "Jerry")
///
Car l'idée du Jerry est de lutter contre le gaspillage accéléré de tous ces composants informatiques qui sont une tragédie pour les pays qui récupère nos déchets ainsi que l'impact sur le réchauffement climatique quand ceux-ci sont brûlés à très haute température.
///
![Jerry](https://blog.linux07.fr/wp-content/uploads/2018/07/jerryjaune.jpg "Jerry")
///
Puis la lutte contre l'obsolescence, car on arrive à obtenir un ordinateur de bureau assez performant pour faire de la bureautique et gérer quelques médias ou bien faire un serveur assez correct avec des pièces jetées.
///
![Jerry](https://blog.linux07.fr/wp-content/uploads/2018/07/emmabuntus-redone-pc-bons-casse.jpg "Jerry")
===
### Installation d'une Bibliobox
(ou Piratebox)
///
![Librarybox](https://blog.linux07.fr/wp-content/uploads/2018/07/librarybox.jpeg "Librarybox")
///
Il s’agit d’un dispositif nomade permettant d’accéder à des ressources numériques ne nécessitant pas de connexion internet.
La Bibliobox permet d’échanger des contenus librement.
///
En outre, ce projet est soucieux de la confidentialité des utilisateurs. Aucune donnée n’est collectée si un utilisateur se connecte.
La Bibliobox peut s’utiliser dans différents contextes notamment en bibliothèque.
C’est l’occasion de mettre à disposition du public des oeuvres du domaine public mais également des contenus sous Creative Commons ou encore sous licences ouvertes.
///
![Librarybox](https://blog.linux07.fr/wp-content/uploads/2018/07/biblioboxmurale2.jpeg "Librarybox")
///
La Bibliobox peut accueillir des oeuvres créées ou réalisées -sous licences ouvertes – par les bibliothèques, tout comme elle a vocation à diffuser des documents à caractère professionnel pendant des journées d’étude et des stages.De manière plus interactive, elle peut également servir de lieu d’échange entre lecteurs et fréquenteurs d’une bibliothèque, au cours d’opérations de médiations numériques.
///
![Pirate Box](https://blog.linux07.fr/wp-content/uploads/2018/07/piratebox.jpeg "Piratebox")
///
Une PirateBox est un dispositif électronique souvent composé d’un routeur et d’un dispositif de stockage d’information, créant un réseau sans fil qui permet aux utilisateurs qui y sont connectés d’échanger des fichiers anonymement et de manière locale.
///
Par définition, ce dispositif qui est souvent portable, est déconnecté d’internet.Les PirateBox sont à l’origine destinées à échanger librement des données libres du domaine public ou sous licence libre. Les logiciels utilisés pour la mise en place d’une PirateBox sont majoritairement open source, voire libres.
===
### Découverte du Raspberry Pi
- pour des enfants et de logiciels educatifs de programmation
- pour les adultes aussi avec tous les champs offert par cette technologie
///
![Raspberry](https://blog.linux07.fr/wp-content/uploads/2018/07/ecranraspberry.png "Raspberry")
///
Le Raspberry Pi est un nano-ordinateur monocarte à processeur ARM conçu par le créateur de jeux vidéo David Braben, dans le cadre de sa fondation Raspberry Pi2.
///
![Raspberry](https://blog.linux07.fr/wp-content/uploads/2018/07/raspberryconnect.jpeg "Raspberry")
///
Cet ordinateur, qui a la taille d'une carte de crédit, est destiné à encourager l'apprentissage de la programmation informatique; il permet l'exécution de plusieurs variantes du système d'exploitation libre GNU/Linux et des logiciels compatibles.
///
![Raspberry](https://blog.linux07.fr/wp-content/uploads/2018/07/framboise.jpeg "Raspberry")
///
Il est fourni nu (carte mère seule, sans boîtier, alimentation, clavier, souris ni écran) dans l'objectif de diminuer les coûts et de permettre l'utilisation de matériel de récupération.
===
### RailsGirls
Atelier pour aborder comment créer une application  et découverte d'un language de programmation
///
![Rails](https://blog.linux07.fr/wp-content/uploads/2018/07/railsgirls.jpeg "Rails")
///
RailsGirls : formation introductive au langage Ruby et au framework de développement Web Ruby.
Ruby on Rails est très utilisé car ce framework apporte un gain en productivité et agilité dans la conception d’applications web.
///
![Rails](https://blog.linux07.fr/wp-content/uploads/2018/07/ruby.jpeg "Rails")
///
Il y a aussi beaucoup de startups web qui se sont construites sur Ruby on Rails comme : Scribd (plus de 70 millions de lecteurs chaque mois), Groupon ou encore Basecamp. Il est aussi utilisé dans de célèbres sociétés comme : Amazon, Apple, BBC, Cisco, CNET, IBM, NASA, Yahoo… !
///
![Rails](https://blog.linux07.fr/wp-content/uploads/2018/07/rails.png "Rails")
///
Cette formation vous permettra d'acquérir les bases du développement web avec Ruby on Rails.
===

### Guide d'Autodéfense de Courrier électronique
///
Enseigner comment utiliser le chiffrage de courrier électronique logiciel libre.
///
![Enigmail](https://blog.linux07.fr/wp-content/uploads/2018/07/infographic-button.png "Enigmail")
///
La surveillance de masse viole nos droits fondamentaux et fait planer un risque sur la liberté d'expression.
///
Apprendre les bases d'une méthode d'autodéfense contre la surveillance :
le chiffrement du courriel.
///
![Enigmail](https://blog.linux07.fr/wp-content/uploads/2018/07/step2a-01-make-keypair.png "Enigmail")
///
Une fois que vous l'aurez assimilée, vous serez en mesure d'envoyer et de recevoir des courriels codés, et ainsi faire en sorte qu'un outil de surveillance ou un voleur qui les intercepterait ne puisse pas les lire. Tout ce dont vous avez besoin, c'est d'un ordinateur doté d'une connexion à Internet, d'un compte de courriel et d'environ une demi-heure.
///
![Enigmail](https://blog.linux07.fr/wp-content/uploads/2018/07/section3-try-it-out.png "Enigmail")
===
## Besoins

Pour ces évènement, nous avons besoin d'une structure, l'idéal serait un centre multimédia, ou une bibliothèque.
///
Il nous faudrait  :
- une connexion internet (indipensable)
- des tables et des chaises
- un vidéo-projecteur
- des écrans (si possible)
///
### Merci
===
Le propos de la communauté et de la culture du logiciel libre est un enjeu majeur pour l'évolution de notre société, surtout dans le domaine public.
Je vous rappelle que le gouvernement l'utilise et le préconise.
///
### Les chiffres ne mentent pas :
le logiciel libre est partout
///
Depuis le début du siècle, le logiciel libre est à la source de toutes les révolutions technologiques, de l’avènement de l’infonuagique à l’Internet des objets, en passant par le Big Data et l’impression 3D. Il fut également un élément clé dans la réussite des grandes organisations du Web – Amazon, Google, Twitter, Ali Baba, Facebook, etc....
///
La participation en grand nombre de chefs de la direction, de responsables des technologies de l’information et de vice président(e)s au dernier sondage intitulé « The Future of Open Source » renforce mon positionnement et témoigne du fait que le logiciel libre répond aujourd’hui adéquatement à des besoins d’affaires. Autrefois ignoré, il est maintenant considéré comme un outil stratégique dans plus de 78 % des organisations.
///
Je vous propose d’analyser plus en détails les résultats du sondage afin de mieux comprendre l’état actuel de l’adoption du logiciel libre en entreprise :
///
 - Dans le cadre de projets TI, le logiciel libre est systématiquement prioritaire pour 66 % des répondants;
 - 67 % des organisations avec plus de 5 000 employés contribuent à des projets de logiciels libres;
 - 93 % des organisations ont augmenté leur implication dans des projets libres au cours des douze derniers mois;
 ///
 - 64 % des organisations contribuent à des projets de logiciels libres, une augmentation de 14 % par rapport à l’année précédente.
 - Également, 88 % planifient augmenter leur niveau de participation au cours des deux à trois prochaines années.
 ///
 Selon le sondage, les raisons principales qui justifient cet engouement sont les suivantes :
///
 - 50 % croient que le logiciel libre leur permet d’attirer les meilleures ressources techniques;
 - 65 % croient que le logiciel libre génère un avantage compétitif;
 - 58 % pensent que le logiciel libre est plus évolutif que ses alternatives propriétaires;
 ///
 - 55 % pensent que le logiciel libre est plus sécuritaire que ses alternatives propriétaires et que cette tendance va s’accentuer au cours des deux ou trois prochaines années;
 - 90 % pensent que le logiciel libre est un moteur d’innovation et qu’il permet d’accélérer les délais des déploiements;
 - 78 % pensent que le logiciel libre leur permet d’améliorer leurs marges.
///
Pour conclure, je citerai Dries Buytaert, fondateur du gestionnaire de contenu libre Drupal utilisé par la STM, la Ville de Montréal, Guardian Life, Los Angeles, Pfizer, NBC, BBC, etc... :
///
« Les chiffres ne mentent pas. Cette année a une nouvelle fois était synonyme de succès pour le logiciel libre, que ce soit au niveau de l’adoption dans les grandes entreprises comme dans les PME. Le sondage confirme ce que nous savons depuis toujours – le logiciel libre est un moteur de l’innovation[…] »

 source : http://www.directioninformatique.com/blogue/les-chiffres-ne-mentent-pas-le-logiciel-libre-est-partout/35320
 ===
 ![GNU meditate](https://blog.linux07.fr/wp-content/uploads/2018/07/meditate.jpg "GNU meditate")