# Propositions-linux07.js

Une présentation des projets que l'association Linux07 aimerait développer. Pour cette présentation, le framework [reveal.js](https://github.com/hakimel/reveal.js) est utilisé comme base. La page de présentation est ensuite éditée en Mardown... Je vous laisse lire toute la documentation de ce framework sur son dépôt source...

Vous pouvez consulter cette présentation ici :
[Propositions Linux07](https://rodinux.frama.io/propositions-linux07.js)

# reveal.js [![Build Status](https://travis-ci.org/hakimel/reveal.js.svg?branch=master)](https://travis-ci.org/hakimel/reveal.js) <a href="https://slides.com"><img src="https://s3.amazonaws.com/static.slid.es/images/slides-github-banner-320x40.png?1" alt="Slides" width="160" height="20"></a>

A framework for easily creating beautiful presentations using HTML. [Check out the live demo](http://revealjs.com/).


